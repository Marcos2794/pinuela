<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackpacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backpacks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('paystatus')->default(false);
            $table->string('country');
            $table->string('phone');
            $table->string('email');
            $table->string('description');
            $table->string('guide_name');
            $table->string('atraction_name');
            $table->date('day');
            $table->unsignedInteger('user_id')->nullable()->unsigned();
            $table->integer('atraction_id');
            $table->integer('quantity');
            $table->float('price');
            $table->float('total');
            $table->string('duration');
            $table->string('hour');
            $table->string('image');
            $table->string('type_atraction');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('backpacks');
    }
}
