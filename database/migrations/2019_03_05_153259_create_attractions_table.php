<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atractions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->string('description');
            $table->string('image');
            $table->string('type_atraction');
            $table->time('hour');
            $table->integer('duration');
            $table->unsignedInteger('guide_id');
            $table->integer('spaces');
            $table->integer('stock');
            $table->date('day');
            $table->boolean('status')->default(true);
            $table->foreign('guide_id')->references('id')->on('guides');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attractions');
    }
}
