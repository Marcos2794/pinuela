<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminNotification extends Mailable
{
    use Queueable, SerializesModels;
    public $subject ;
    public $description ;
    public $guide_name ; 
    public $quantity ;
    public $price;
    public $duration;
    public $hour;
    public $atraction_name;
    public $total;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->description = $message->description;
        $this->guide_name = $message->guide_name; 
        $this->quantity = $message->quantity;
        $this->price = $message->price;
        $this->duration = $message->duration;
        $this->hour = $message->hour;
        $this->atraction_name = $message->atraction_name;
        $this->total = intval($message->price) * intval($message->quantity);
        $this->user = $message->name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.admin_notification');
    }
}
