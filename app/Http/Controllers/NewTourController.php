<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Atraction;
use App\Guide;
use App\Http\Requests\AttractionsPost;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class NewTourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {    
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $this->middleware('auth');
                $this->middleware('admin');
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }
    public function index()
    {
         try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                return view('admin.newtour.index', ['tours' =>Atraction::select()
                ->where('type_atraction','=', 'Tour')
                ->orderBy('created_at','desc')
                ->paginate(5),'guides' => Guide::all()]);
            } 
        } catch (\Exception $e) { 
        
            abort(500, 'No conection');
        
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                return view('admin.newtour.create',['guides' => Guide::all()]);
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');
            }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttractionsPost $request)
    {
       
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $tour = New Atraction();
                //si el request me retorna una imagen(archivo file), lo guarda en la ruta img
                if($request->hasFile('image')){
                    $file = $request->file('image');
                    $name = time().$file->getClientOriginalName();
                    $tour->image = $name;
                    $file ->move(public_path().'/img/',$name);
                }
                //guardo todos los valores del request excepto la image que la cargo asi = $tour->image = $name;
                $tour->type_atraction = 'Tour';
                $tour->stock = Input::get('stock');
                $tour->fill($request->except('image'))->save();
                return redirect('admin/newtour')->withSuccess('tour creado con éxito'); 
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');
            }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                // get the tour
                $tour = Atraction::find($id);

                // show the edit form and pass the nerd
                return view('admin.newtour.edit',['guides' => Guide::all()])->with('tour', $tour);
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');
            }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AttractionsPost $request, $id)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            {    
                $tour = Atraction::find($id);
                //si el request me retorna una imagen(archivo file), lo guarda en la ruta img
                if($request->hasFile('image')){
                    $file = $request->file('image');
                    $name = time().$file->getClientOriginalName();
                    $tour->image = $name;
                    $file ->move(public_path().'/img/',$name);
                }
                // process the register
                
                    $tour->name = Input::get('name');
                    $tour->description  = Input::get('description');
                    $tour->price   = Input::get('price');
                    $tour->spaces   = Input::get('spaces');
                    $tour->stock   = Input::get('stock');
                    $tour->duration   = Input::get('duration');
                    $tour->hour   = Input::get('hour');
                    $tour->guide_id   = Input::get('guide_id');
                    $tour->day   = Input::get('day');
                    $tour->status   = Input::get('status');
                    $tour->fill($request->except('image'))->save();

                    // redirect
                    return redirect('admin/newtour');
                } 
            } catch (\Exception $e) { 
    
                abort(500, 'No conection');
                }
         
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                 // delete
                DB::table('atractions')->where('id', '=', $id)->delete();

                // redirect  
                return redirect('admin/newtour')->withSuccess('tour delete succesfuly'); 
            } 
        } catch (\Exception $e) { 
        
            abort(404, 'No conection');
        
            }
    }
}

