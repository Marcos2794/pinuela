<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Backpack;
use App\User;
use DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('admin');
    }
    
    public function index()
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                return view('admin.index',
                [
                    'totaltours' => BackPack::select(
                    DB::raw("SUM(quantity) as quantity, SUM(price) as price"))
                    ->where('paystatus','=', '1')
                    ->where('type_atraction','=', 'Tour')->get(),
                    
                    'tours' =>  BackPack::select( DB::raw('atraction_name, count(atraction_name) as total'))
                    ->where('type_atraction','=', 'Tour')
                    ->where('paystatus','=', '1')
                    ->orderBy('total','desc')->groupBy('atraction_name')->paginate(5),
                    
                    'users'  => User::select( DB::raw('country, count(country) as total'))->where('admin','=', '0')
                    ->orderBy('total','desc')->groupBy('country')->paginate(5),

                    'totalusers' => User::select( DB::raw('count(id) as id'))->where('admin','=', '0')
                    ->get(),

                    'packages' => BackPack::select( DB::raw('atraction_name, count(atraction_name) as total'))
                    ->where('type_atraction','=', 'Package')
                    ->where('paystatus','=', '1')
                    ->orderBy('total','desc')->groupBy('atraction_name')->paginate(5),

                    'totalpackages' => BackPack::select(DB::raw("SUM(quantity) as quantity, SUM(price) as price"))
                    ->where('paystatus','=', '1')
                    ->where('type_atraction','=', 'Package')->get()]);
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}