<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use Illuminate\Support\Facades\DB;
use App\Atraction;
use App\BackPack;

use Illuminate\Support\Facades\Auth;
use App\Mail\ReservationMail;
use App\Mail\AdminNotification;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ReservationPost;

class PaypalController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
 
    public function payWithpaypal(ReservationPost $request)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $atraction = DB::table('atractions')
                ->where('id', $request->get('atraction_id'))->first();
                if ($atraction->stock >= $request->get('quantity')) {

                    DB::table('atractions')->where('id', $request->get('atraction_id'))
                    ->update(['stock' => $atraction->stock - $request->get('quantity')]);

                   

                    $backpack = new \App\BackPack;
                    //if the user are loged insert usert id in the table backpack
                    if (Auth::user()) {
                        $backpack->user_id=Auth::user()->id; 
                        $backpack->phone=$request->get('phone');
                        $backpack->country=$request->get('country');
                    }else{
                        $countries = DB::table('countries')
                        ->where('phone_code', $request->get('country'))->first();

                        $backpack->phone='('.$request->get('country').')'.$request->get('phone');
                        $backpack->country=$countries->name;
                        $backpack->user_id = null;
                    }
                    $backpack->atraction_id=$request->get('atraction_id');
                    $backpack->guide_name=$request->get('guide_name');
                    $backpack->name=$request->get('fullname');
                    $backpack->email=$request->get('email');
                    $backpack->description=$request->get('description');
                    $backpack->quantity=$request->get('quantity');
                    $backpack->price=$request->get('price');
                    $backpack->total=$request->get('total');
                    $backpack->duration=$request->get('duration');
                    $backpack->hour=$request->get('hour');
                    $backpack->image=$request->get('image');
                    $backpack->day=$request->get('day');
                    $backpack->atraction_name=$request->get('atraction_name');
                    $backpack->type_atraction=$request->get('type_atraction');
                    $backpack->save();
                    
                    // send email notification for Admin, about reservation 
                    Mail::to('pinuelatreks@gmail.com')->queue(new AdminNotification($backpack));
                }else{
                    return back()->withErrors(['There is no spaces in this attraction for this date']);
                }

            }
        } catch (\Exception $e) { 
        
                abort(500, 'No conection');
        }

        $quantity = $request->get('quantity');

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $item_1->setName($request->get('name')) /** item name **/
            ->setCurrency('USD')
            ->setQuantity($request->get('quantity'))
            ->setPrice($request->get('price')); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($request->get('price') * $request->get('quantity'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('status',$backpack->id)) /** Specify return URL **/
            ->setCancelUrl(URL::to('status',$backpack->id));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {

                //if error update atractions table again
                DB::table('atractions')->where('id', $request->get('atraction_id'))
                    ->update(['stock' => $atraction->stock + $request->get('quantity')]);

                    //if error delete the new backpack
                    $backpack->delete();

                \Session::put('error', 'Connection timeout');
                return Redirect::to('/');
            } else {
                //if error update atractions table again
                DB::table('atractions')->where('id', $request->get('atraction_id'))
                ->update(['stock' => $atraction->stock + $request->get('quantity')]);

                //if error delete the new backpack
                $backpack->delete();
                
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');

        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                DB::table('stocks')->where('atraction_id', $request->get('atraction_id'))
                ->whereDate('date', $request->get('day'))
                ->update(['stocks' => $stocks->stock + $request->get('quantity')]);
                $backpack->delete();
                return Redirect::to('/');
            }
        } catch (\Exception $e) { 
        
                abort(500, 'No conection');
        }
    }
    public function getPaymentStatus($backpack_id)
    {
        $backpack = DB::table('backpacks')
        ->where('id', $backpack_id)->first();
        $atraction = Atraction::find($backpack->atraction_id);

        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            
            $backpack = DB::table('backpacks')
            ->where('id', $backpack_id)->first();
            $atraction = Atraction::find($backpack->atraction_id);
            //if error update atractions table again
            DB::table('atractions')->where('id',  $backpack->atraction_id)
            ->update(['stock' => $atraction->stock + $backpack->quantity]);

            //if error delete the new backpack
            $backpack->delete();

            return Redirect::to('/');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved') {

            DB::table('backpacks')->where('id',  $backpack_id)
            ->update(['paystatus' => true]);
            
            Mail::to($backpack->email)->queue(new ReservationMail($backpack));

            if (Auth::user()) {
                return redirect('/client/backpack')->with('success', 'Information has been added');
            }else{
                return redirect('/')->with('success', 'Information has been added');
            }
        }
        \Session::put('error', 'Payment failed');

        //if error update atractions table again
        DB::table('atractions')->where('id', $backpack_id)
        ->update(['stock' => $atraction->stock + $backpack->quantity]);

        //if error delete the new backpack
        $backpack->delete();

        return Redirect::to('/');
    }
    
}