<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Backpack;
use App\Http\Requests\TourPost;
use Illuminate\Support\Facades\Auth;

class BackpackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            {  
                $userid = Auth::id();
                return view('client.backpack.index', ['atractions'=> Backpack::where('user_id','=', $userid)
                ->orderBy('created_at','desc')
                ->where('paystatus','=', 1)
                ->paginate(4)]);
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $atraction = DB::table('backpacks')->find($id);
                

                if($atraction == null){
                    abort(500, 'No conection');
                }
                return view('client.backpack.show', ['backpacks' => $atraction]);
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
