<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Country;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller
{
    
    public function index()
    {
        
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $user = Auth::id();

                // show the edit form and pass the nerd
                return view('client.profile.index')->with('user', $user);;
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');
            }
    }
     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                // get the tour
                $user = User::find($id);

                // show the edit form and pass the nerd
                return view('client.profile.show')->with('user', $user);;
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');
            }
    }

    public function edit($id)
    {
        
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                // get the tour
                $user = User::find($id);

                // show the edit form and pass the nerd
                return view('client.profile.edit',['countries' => Country::all()])->with('user', $user);
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');
            }
    }

    public function editpass($id)
    {
        
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                if(Input::get('password') == null){
                    $countries = DB::table('countries')
                    ->where('phone_code', Input::get('phone_code'))->first();
                    // validate
                    // read more on validation at http://laravel.com/docs/validation
                    $rules = array(
                        'name' => ['required', 'string', 'max:255'],
                        'email' => ['required', 'string', 'email', 'max:255'],
                        'phone' => ['required', 'string'],
                    );
                    $user = User::find($id);

                    // process the update
                    
                        $user->name = Input::get('name');
                        $user->email  = Input::get('email');
                        $user->phone = '('.Input::get('phone_code').')'.Input::get('phone');
                        $user->country  = $countries->name;
                        $user->update();

                    // redirect
                    
                    return redirect('client/profile');
                }
                $rules = array(
                    'password' => ['required', 'string', 'min:6', 'confirmed'],
                );
                
                $user = User::find($id);
                if(Input::get('password')!=Input::get('password_confirmation')){
                    return back()->withErrors('password are wrong');
                }
                // process the update
                
                $user->password = Hash::make(Input::get('password'));
                $user->update();
                return redirect('client/profile')->withSuccess('Change Password Succes');;

            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatepass(Request $request, $id)
    {
        
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                // validate
                // read more on validation at http://laravel.com/docs/validation
                $rules = array(
                    'password' => ['required', 'string', 'min:6', 'confirmed'],
                );
                $user = User::find($id);

                // process the update
                    $user->password = Hash::make($data['password']);
                    $user->update();

                    // redirect
                    
                    return redirect('client/profile');
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }
}