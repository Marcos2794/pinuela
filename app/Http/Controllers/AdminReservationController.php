<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Backpack;
use App\User;
use DB;

class AdminReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            {  
                return view('admin.reservation.index', 
                ['reservations'=> Backpack::select() 
                ->orderBy('created_at','desc')
                ->paginate(5)]);

            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                 // delete
                DB::table('backpacks')->where('id', '=', $id)
                ->where('paystatus', '=', '0')->delete();

                // redirect  
                return redirect('admin/reservation')->withSuccess('tour delete succesfuly'); 
            } 
        } catch (\Exception $e) { 
        
            abort(404, 'No conection');
        
            }
    }
}
