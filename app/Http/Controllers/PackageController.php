<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atraction;
use App\Http\Requests\AttractionsPost;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $packages = DB::table('atractions')
                ->where('type_atraction','=', 'Package')
                ->where('status','=', '1')->paginate(2);
                return view('packages.index', ['packages' => $packages]);
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $packages = DB::table('atractions')->find($id);

                if($packages == null){
                    abort(500, 'No conection');
                }
                return view('packages.show', ['packages' => $packages]);
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

