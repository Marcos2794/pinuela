<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atraction;
use App\User;
use App\Country;
use App\BackPack;
use App\Guide;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AttractionsPost;
use Illuminate\Support\Facades\DB;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $atractions = DB::table('atractions')->find($id);

                if($atractions == null){
                    abort(500, 'No conection');
                }
                return view('reservation.index', ['atractions' => $atractions, 'guides' => Guide::all(), 'countries' => Country::all()]);
            } 
        } catch (\Exception $e) { 

            abort(500, 'No conection');

            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
