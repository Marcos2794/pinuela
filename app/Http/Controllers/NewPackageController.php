<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Atraction;
use App\Guide;
use App\Http\Requests\AttractionsPost;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManager;

class NewPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            {        
                $this->middleware('auth');
                $this->middleware('admin');
            } 
        } catch (\Exception $e) { 
        
            abort(500, 'No conection');
        
            }
    }
    
    public function index()
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                return view('admin.newpackage.index', ['packages' =>Atraction::select()
                ->where('type_atraction','=', 'Package')
                ->orderBy('created_at','desc')
                ->paginate(5), 'guides' => Guide::all()]);
            } 
        } catch (\Exception $e) { 
        
            abort(500, 'No conection');
        
            }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                return view('admin.newpackage.create',['guides' => Guide::all()]);
            } 
        } catch (\Exception $e) { 
        
            abort(500, 'No conection');
        
            }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttractionsPost $request)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                $package = New Atraction();
                //si el request me retorna una imagen(archivo file), lo guarda en la ruta img
                if($request->hasFile('image')){
                    $file = $request->file('image');
                    $name = time().$file->getClientOriginalName();
                    $package->image = $name;
                    $file ->move(public_path().'/img/',$name);
                }
                //guardo todos los valores del request excepto la image que la cargo asi = $package->image = $name;
                $package->type_atraction = 'Package';
                $package->stock = Input::get('stock');
                $package->fill($request->except('image'))->save();
                return redirect('admin/newpackage/')->with('info', 'Producto creado con éxito');
            } 
        } catch (\Exception $e) { 
        
            abort(500, 'No conection');
        
            } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                // get the tour
                $package = Atraction::find($id);

                // show the edit form and pass the nerd
                return view('admin.newpackage.edit',['guides' => Guide::all()])->with('package', $package);
            } 
        } catch (\Exception $e) { 
        
            abort(500, 'No conection');
        
            }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AttractionsPost $request, $id)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 

                $package = Atraction::find($id);
                //si el request me retorna una imagen(archivo file), lo guarda en la ruta img
                if($request->hasFile('image')){
                    $file = $request->file('image');
                    $name = time().$file->getClientOriginalName();
                    $package->image = $name;
                    $file ->move(public_path().'/img/',$name);
                }
                // process the register
                    $package->name = Input::get('name');
                    $package->description  = Input::get('description');
                    $package->price   = Input::get('price');
                    $package->spaces   = Input::get('spaces');
                    $package->stock   = Input::get('spaces');
                    $package->duration   = Input::get('duration');
                    $package->hour   = Input::get('hour');
                    $package->guide_id   = Input::get('guide_id');
                    $package->day   = Input::get('day');
                    $package->status   = Input::get('status');
                    $package->fill($request->except('image'))->save();

                    // redirect
                    return redirect('admin/newpackage');
            } 
        } catch (\Exception $e) { 
        
            abort(500, 'No conection');
        
            }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                    // delete
                    DB::table('atractions')->where('id', '=', $id)->delete();
        
                // redirect
                
                return redirect('admin/newpackage')->with('info', 'paquete eliminado con éxito');
            } 
        } catch (\Exception $e) { 
        
            abort(500, 'No conection');
        
            }
    }
}