<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    { 
        try { DB::connection()->getPdo(); 

            if(DB::connection()->getDatabaseName())
            { 
                if (! $request->expectsJson()) {
                    return route('login');
                }
            }
        } catch (\Exception $e) { 

            abort(500, 'No conection');
            }
        }
}
