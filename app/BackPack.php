<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackPack extends Model
{
    protected $fillable = [
        'guide_name', 'user_id', 'name','paystatus','country','phone','email','description','total','quantity', 'duration', 'hour','image', 
    ];

    protected $table = 'backpacks';

    public function scopeName($query, $name)
    {
        if($name)
            return $query->where('name', 'LIKE', "%$name%");
    }
}