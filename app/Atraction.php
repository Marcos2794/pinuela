<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atraction extends Model
{
    protected $fillable = [
        'name', 'price', 'description', 'image', 'type_atraction', 'hour', 'duration', 'guide_id', 'spaces','stock','day','status'
        
   ];
   
}
