@extends('layouts.app')
<title>Tours</title>
@section('content')
<!-- Two -->
<br>
<br>
<br>
<section id="two" class="wrapper alt style2">
    <section class="spotlight">
        <div class="image"><img src="/img/{{$packages->image}}" alt="" /></div>
        <div class="content">
            <h2>{{$packages->name}}</h2>
            <ul>
                <li style="list-style:none;"><i class="fas fa-hiking"></i> {{$packages->description}}</li>
                <li style="list-style:none;"><i class="fas fa-hiking"></i> Date: {{$packages->day}}</li>
                <li style="list-style:none;"><i class="fas fa-hiking"></i> Spaces: {{$packages->stock}}</li>
                <li style="list-style:none;"><i class="fas fa-hiking"></i> $ {{$packages->price}}</li>
            </ul>
            <a href="/book/{{$packages->id}}" class="btn btn-sm  btn-warning"><i class="fab fa-earlybirds"></i>Book and
                Pay</a>
            <br>
        </div>
    </section>
</section>
@endsection