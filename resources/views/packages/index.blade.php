@extends('layouts.app')
<title>Packages</title>
    @section('content')
    <section id="one" class="wrapper style1 special">
	<div class="inner">
		<header class="major">
            <h2>Packages</h2>
                <p>Designed for nature lovers whom prefer authentic experiences without too much the same.<br /></p>
		</header>
	</div>
</section>

    <!-- Three -->
    <section id="three" class="wrapper style3 special">
        <div class="inner">
            <ul class="features">
                @foreach($packages as $package)
                @if($package->status == 1)
                    <li><img src="/img/{{$package->image}}" class="imagen" />
                        <h3>{{$package->name}}</h3>
                        <p>$ {{$package->price}}</p>
                        <p>Date: {{$package->day}}</p>
                        <p>Spaces: {{$package->stock}}</p>
                        <p>{{substr($package->description, 0, 80)}} ...</p>
                        <a href="/packages/{{$package->id}}" class="btn btn-sm  btn-primary"><i class="fas fa-binoculars"></i>See More</a>
                        <a href="/book/{{$package->id}}" class="btn btn-sm  btn-warning"><i class="fab fa-earlybirds"></i>Book and Pay</a>
                    </li>
                @endif
                @endforeach
            </ul>
        </div>
    </section>
@endsection


