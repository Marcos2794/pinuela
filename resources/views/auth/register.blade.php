@extends('layouts.app') @section('content')
<dvi class="container h-100">
    <div class="d-flex justify-content-center">
        <div class="card mt-5 col-md-4 animated bounceInDown myForm">
            <div class="card-header">
                <h4>Piñuela Login</h4>
            </div>
            <div class="card-body">
                {!! Form::open(['url' => 'register' , 'files' => 'true','method' => 'POST']) !!}
                @csrf
                <div id="dynamic_container">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                                <i class="fas fa-user"></i>
                            </span>
                        </div>
                        <input id="name" type="text" placeholder="Fullname"
                            class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                            value="{{ old('name') }}" required autofocus> @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                                <i class="fas fa-at"></i>
                            </span>
                        </div>
                        <input id="email" type="email" placeholder="Email"
                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                            value="{{ old('email') }}" required> @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                            <i class="fas fa-globe-americas"></i>
                            </span>
                        </div>
                        <select name="phone_code" class="form-control" id="phone_code" style="height:44px">
                            @foreach($countries as $phone_code)
                            <option value="{{$phone_code['phone_code']}}">{{$phone_code['name']}}
                                ({{$phone_code['phone_code']}})</option>
                            @endforeach
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone_code') }}</strong>
                            </span>
                            <div class="input-group-prepend mt-3">
                                <span class="input-group-text br-15">
                                    <i class="far fa-calendar-alt"></i>
                                </span>
                            </div>
                            {{ Form::text('phone', null, array('class' => 'form-control','placeholder'=>'Phone','min'=>'1', 'required')) }}
                    </div>
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                                <i class="fas fa-key"></i>
                            </span>
                        </div>
                        <input id="password" type="password" placeholder="Password"
                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                            required> @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                                <i class="fas fa-lock-open"></i>
                            </span>
                        </div>
                        <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control"
                            name="password_confirmation" required>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success btn-sm float-right submit_btn">
                    <i class="fas fa-arrow-alt-circle-right"></i>Sing In</button>
                <a href='/login' class="btn btn-secondary btn-sm" id="add_more">
                    <i class="fas fa-plus-circle"></i>Login</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</dvi>
@endsection