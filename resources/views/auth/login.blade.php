@extends('layouts.app') @section('content')
<dvi class="container h-100">
	<div class="d-flex justify-content-center">
		<div class="card mt-5 col-md-4 animated bounceInDown myForm">
			<div class="card-header">
				<h4>Piñuela Login</h4>
			</div>
			<div class="card-body">
				{!! Form::open(['url' => 'login' , 'files' => 'true','method' => 'POST']) !!}
				<div id="dynamic_container">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
								<i class="fas fa-user"></i>
							</span>
						</div>
						<input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
						 value="{{ old('email') }}" required autofocus>
					</div>
					<div class="input-group mt-3">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
								<i class="fas fa-key"></i>
							</span>
						</div>
						<input id="password" type="password" placeholder="Password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
						 name="password" required>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<button class="btn btn-success btn-sm float-right submit_btn">
					<i class="fas fa-arrow-alt-circle-right"></i> Login</button>
				<a href='/auth/register' class="btn btn-secondary btn-sm" id="add_more">
					<i class="fas fa-plus-circle"></i> Register</a>
			</div>
			{!! Form::close() !!}
			<a href='/reset'> Forgot Password</a>
		</div>
	</div>
</dvi>

@endsection
