@extends('layouts.app')
<title>Email Verify</title>
    @section('content')
    <!-- Three -->
    <section id="three" class="wrapper style3 special">
    <div class="card-header">{{ __('Verify Your Email Address') }}</div>
        <div class="inner">
            <ul class="features">
                    <li>
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                    </li>
            </ul>
        </div>
    </section>
@endsection
