 @extends('layouts.app') @section('content')
<dvi class="h-100">
	<div class="d-flex justify-content-center">
		<div class="card mt-5 col-md-4 animated bounceInDown myForm">
			<div class="card-header">{{ __('Reset Password') }}</div>

			<div class="card-body">
				<form method="POST" action="{{ route('password.update') }}">
					@csrf

					<input type="hidden" name="token" value="{{ $token }}">
					<div id="dynamic_container">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text br-15">
									<i class="fas fa-user"></i>
								</span>
							</div>
							<input id="email" type="email" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
							 value="{{ old('email') }}" required autofocus>
						</div>
						<div class="input-group mt-3">
							<div class="input-group-prepend">
								<span class="input-group-text br-15">
									<i class="fas fa-user"></i>
								</span>
							</div>
							<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
							 required> @if ($errors->has('password'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
						<div class="input-group mt-3">
							<div class="input-group-prepend">
								<span class="input-group-text br-15">
									<i class="fas fa-user"></i>
								</span>
							</div>
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
						</div>
					</div>
					<div class="card-footer">
						<button class="btn btn-secondary btn-block" id="add_more">
							<i class="fas fa-arrow-alt-circle-right"></i>
							{{ __('Send Password Reset Link') }}
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</dvi>
@endsection