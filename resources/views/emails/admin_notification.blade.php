<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>holi</title>
</head>

<body style="background-color: white ">

    <!--Copia desde aquí-->
    <table style="max-width: 600px; padding: 10px; margin:0 auto; border-collapse: collapse;">
        <tr>
            <td style="padding: 0">
                <img style="padding: 0; display: block" src="https://i.postimg.cc/Gh96J7Xt/pic01.jpg" width="100%">
            </td>
        </tr>
        <tr>
            <td style="background-color: #ecf0f1">
                <div style="color: #34495e; margin: 4% 10% 2%; text-align: left;font-family: sans-serif">
                <h1 style="color: #e67e22; margin: 0 0 7px">Piñuela Treks</h1>
                    <p style="margin: 2px; font-size: 15px">
                    We have a new resevation to {{$user}}, go to the page an verify the user pay status</p>
                    <ul style="font-size: 15px;  margin: 10px 0">
                        <h2>{{$atraction_name}}</h2>
                        <li>{{$description}}</li>
                        <li>Guide: {{$guide_name}}</li>
                        <li>Reserved Spaces: {{$quantity}}</li>
                        <li>Price per person: {{$price}}</li>
                        <li>Total price: {{$total}}</li>
                        <li>Days: {{$duration}}</li>
                        <li>Start at: {{$hour}}</li>
                    </ul>
                    <div style="width: 100%;margin:20px 0; display: inline-block;text-align: center">
                        <img style="padding: 0; width: 200px; margin: 5px"
                            src="https://i.postimg.cc/Gh96J7Xt/pic01.jpg">
                        <img style="padding: 0; width: 200px; margin: 5px"
                            src="https://i.postimg.cc/RV8yP7YR/pic03.jpg">
                    </div>
                    <div style="width: 100%; text-align: center">
                        <a style="text-decoration: none; border-radius: 5px; padding: 11px 23px; color: white; background-color: #3498db"
                            href="http://localhost:8002">Go to page</a>
                    </div>
                    <p style="color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0">Piñuela Treks
                        <?php echo date("Y"); ?></p>
                </div>
            </td>
        </tr>
    </table>
    <!--hasta aquí-->

</body>

</html>