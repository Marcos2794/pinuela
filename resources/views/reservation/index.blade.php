@extends('layouts.app')
<title>Tours</title>
@section('content')
<!-- Two -->
<dvi class="container h-100">
    <div class="spacing-1"></div>
    <section id="two" class="wrapper alt style2">
        <section class="spotlight">
            <div class="image"><img src="/img/{{$atractions->image}}" alt="" /></div>
            <div class="content">

                <div class="card mt-5 col-lg-8 animated bounceInDown myForm">
                    <div class="card-header">
                        <h2>{{$atractions->name}}</h2>
                        <ul>
                            <li style="list-style:none;"><i class="fas fa-hiking"></i> Start Hour: {{$atractions->hour}}
                            </li>
                            <li style="list-style:none;"><i class="fas fa-hiking"></i> Price per person $
                                {{$atractions->price}}</li>
                            <li style="list-style:none;"><i class="fas fa-hiking"></i> {{$atractions->stock}} Spaces
                                available on {{$atractions->day}}</li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <form method="POST" id="payment-form" action="{!! URL::to('paypal') !!}">
                            {{ csrf_field() }}
                            <div id="dynamic_container">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text br-15">
                                            <i class="fas fa-user"></i>
                                        </span>
                                    </div>
                                    @if (Auth::guest())
                                    {{ Form::text('fullname', null, array('class' => 'form-control', 'placeholder'=>'Fullname', 'style'=>'width:70px', 'id' => 'fullname', 'required')) }}
                                    @else
                                    {{ Form::text('fullname', auth()->user()->name, array('class' => 'form-control','readonly', 'style'=>'width:70px', 'id' => 'fieldblack', 'required')) }}
                                    @endif
                                </div>
                                <div class="input-group mt-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text br-15">
                                            <i class="fas fa-at"></i>
                                        </span>
                                    </div>
                                    @if (Auth::guest())
                                    {{ Form::text('email', null, array('class' => 'form-control', 'placeholder'=>'Email', 'style'=>'width:70px', 'id' => 'email', 'required')) }}
                                    @else
                                    {{ Form::text('email', auth()->user()->email, array('class' => 'form-control','readonly', 'style'=>'width:70px', 'id' => 'fieldblack', 'required')) }}
                                    @endif
                                </div>
                                <div class="input-group mt-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text br-15">
                                            <i class="fas fa-globe-americas"></i>
                                        </span>
                                    </div>
                                    @if (Auth::guest())
                                    <select name="country" class="form-control" id="country" style="height:44px">
                                        @foreach($countries as $phone_code)
                                        <option value="{{$phone_code['phone_code']}}">{{$phone_code['name']}}
                                            ({{$phone_code['phone_code']}})</option>
                                        @endforeach
                                        @else
                                        {{ Form::text('country', auth()->user()->country, array('class' => 'form-control','readonly','placeholder'=>'Country','id' => 'fieldblack', 'required')) }}
                                        @endif
                                        <div class="input-group-prepend ">
                                            <span class="input-group-text br-15">
                                                <i class="fas fa-phone"></i>
                                            </span>
                                        </div>
                                        @if (Auth::guest())
                                        {{ Form::text('phone', null, array('class' => 'form-control', 'placeholder'=>'Phone','id' => 'phone','style'=>'height:44px','onkeypress'=>'return justNumbers(event);',  'required')) }}
                                        @else
                                        {{ Form::text('phone', auth()->user()->phone, array('class' => 'form-control', 'placeholder'=>'Phone','readonly','id' => 'fieldblack','style'=>'height:44px', 'required')) }}
                                        @endif
                                </div>
                                <div class="input-group mt-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text br-15">
                                            <i class="fas fa-hiking"></i>
                                        </span>
                                    </div>
                                    {{ Form::number('quantity', null, array('class' => 'form-control', 'placeholder'=>'quantity','id' => 'quantity',
									'style'=>'height:44px','onClick'=>'Total()','required', 'min'=>'1', 'onkeypress'=>'return justNumbers(event);')) }}
                                    <div class="input-group-prepend">
                                        <span class="input-group-text br-15">
                                            <i class="fas fa-dollar-sign"></i>
                                        </span>
                                    </div>
                                    {{ Form::text('total', null, array('class' => 'form-control', 'placeholder'=>'Total', 'readonly','id' => 'total', 'required')) }}
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success btn-sm btn-block float-right submit_btn">
                                    <i class="fas fa-arrow-alt-circle-right"></i> Pay with PAYPAL</button>
                                <a class="btn btn-danger  btn-lg btn-block float-left danger_btn"
                                    onClick="javascript:history.back();">
                                    <i class="fas fa-delete"></i> Cancel</a>
                            </div>
                            <input type="hidden" name="price" value="{{ $atractions->price }}">
                            <input type="hidden" name="type_atraction" value="{{ $atractions->type_atraction }}">
                            <input type="hidden" name="atraction_id" value="{{ $atractions->id }}">
                            <input type="hidden" name="hour" value="{{ $atractions->hour }}">
                            <input type="hidden" name="duration" value="{{ $atractions->duration }}">
                            <input type="hidden" name="day" value="{{ $atractions->day}}">
                            <input type="hidden" name="image" value="{{ $atractions->image }}">
                            <input type="hidden" name="description" value="{{ $atractions->description }}">
                            @foreach($guides as $guide)
                            @if($atractions->guide_id == $guide->id)
                            <input type="hidden" name="guide_name" value="{{ $guide->name }}">
                            @endif
                            @endforeach
                            <input type="hidden" name="atraction_name" value="{{ $atractions->name }}">
                            <input type="hidden" name="price" value="{{ $atractions->price }}" id="price">
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <section id="one" class="wrapper style1 special">
        <div class="inner">
            <header class="major">
                <h2>“All you got to do is decide to go and <br /> the hardest part is over. So, go!”
                    <br /> Tony Wheeler</h2>
            </header>
            <ul class="icons major">
                <li>
                    <span style="font-size: 3em;">
                        <i class="fas fa-frog fa-2xs fa-pull-right fa-border"></i>
                    </span>
                </li>
                <li>
                    <span style="font-size: 3em; color: Green;">
                        <i class="fab fa-pagelines fa-2xs fa-pull-right fa-border"></i>
                    </span>
                </li>
                <li>
                    <span style="font-size: 3em; color: Dodgerblue;">
                        <i class="fas fa-water fa-2xs fa-pull-right fa-border"></i>
                    </span>
                </li>
            </ul>
        </div>
    </section>
</dvi>

@endsection