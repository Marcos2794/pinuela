@extends('layouts.app')
<title>Tours</title>
@section('content')
<!-- Two -->
<br>
<br>
<br>
<section id="two" class="wrapper alt style2">
	<section class="spotlight">
		<div class="image">
			<img src="/img/{{$backpacks->image}}" alt="" />
		</div>
		<div class="content">
			<h2>{{$backpacks->name}}</h2>
			<p>People: {{$backpacks->quantity}}</p>
			<p>{{$backpacks->description}}</p>
			<p>Price/Person $ {{$backpacks->price}}</p>
            <p>Reserved {{$backpacks->created_at}}</p>
            
			<a class="btn btn-sm  btn-danger" onClick="javascript:history.back();">
				<i class="fas fa-delete"></i>Go Back</a>
		</div>
	</section>
</section>

@endsection