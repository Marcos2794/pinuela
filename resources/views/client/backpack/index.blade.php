@extends('layouts.app')
<title>My Atractions</title>
@section('content')
<div class="spacing-1"></div>
<div class="categoria">
	<div class="container">
		@if(!$atractions)
		<br>
		<br>
		<table class="table bg-info table-bordered">
			<tr>
				<th class="text-center">You still do not have anything in your backpack, reserve your first attraction and come back later</th>
			</tr>
		</table>
		<br>
		<br>
		@else
		<br>
		<br>
		<table class="table table-dark table-bordered">
			<tr>
				<th class="text-center">Name</th>
				<th class="text-center">Description</th>
				<th class="text-center">Guide</th>
				<th class="text-center">Peoples</th>
				<th class="text-center">Price per person $</th>
				<th class="text-center">Started Hour</th>
				<th class="text-center">Image</th>
				<th class="text-center">
					<i class="fas fa-plus-circle"></i>
					</a>
				</th>
			</tr>
			<!--Cargo los atributos de productos con el foreach-->
			@foreach($atractions as $atraction)
			<tr>
				<td class="text-center">{{$atraction->name}}</td>
				<td class="text-center">{{$atraction->description}}</td>
				<td class="text-center">{{$atraction->guide_name}}</td>
				<!--Busco el nombre del guia con el id del producto-->

				<td class="text-center">{{$atraction->quantity}}</td>
				<td class="text-center">{{$atraction->price}}</td>
				<td class="text-center">{{$atraction->hour}}</td>
				<td class="text-center">
					<img src="/img/{{$atraction->image}}" class="imagen" />
				</td>
				<td class="text-center">
					<a href="/client/backpack/{{$atraction->id}}" class="btn btn-sm  btn-primary">
						<i class="fas fa-binoculars"></i>See More</a>
				</td>
			</tr>
			@endforeach

		</table>
		<!--Divisor de pagina-->
		{{$atractions->links() }} @endif
	</div>
</div>
</div>
@endsection