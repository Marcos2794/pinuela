@extends('layouts.app') @section('content')
<dvi class="container h-100">
    <div class="d-flex justify-content-center">
        <div class="card mt-5 col-md-4 animated bounceInDown myForm">
            <div class="card-header">
                <h4>Profile</h4>
            </div>
            <div class="card-body">
                {{ Form::model($user, array('route' => array('profile.update',auth()->user()->id), 'method' => 'PUT')) }}
                @csrf
                <div id="dynamic_container">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                                <i class="fas fa-user"></i>
                            </span>
                        </div>
                        {{ Form::text('name',auth()->user()->name, array('class' => 'form-control', 'placeholder'=>'Name','required')) }}
                    </div>
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                                <i class="fas fa-at"></i>
                            </span>
                        </div>
                        {{ Form::text('email', auth()->user()->email, array('class' => 'form-control', 'placeholder'=>'email','required')) }}
                    </div>
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                                <i class="fas fa-at"></i>
                            </span>
                        </div>
                        <select name="phone_code" class="form-control" id="phone_code" style="height:44px">
                            @foreach($countries as $phone_code)
                            @if(auth()->user()->country==$phone_code['name'])
                            <option value="{{$phone_code['phone_code']}}">{{auth()->user()->country}}({{$phone_code['phone_code']}})</option>
                            @endif
                            @endforeach
                            @foreach($countries as $phone_code)
                            <option value="{{$phone_code['phone_code']}}">{{$phone_code['name']}}
                                ({{$phone_code['phone_code']}})</option>
                            @endforeach
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone_code') }}</strong>
                            </span>
                    </div>
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                                <i class="fas fa-at"></i>
                            </span>
                        </div>
                        {{ Form::text('phone', preg_replace("/\((.*?)\)/i", "", auth()->user()->phone), array('class' => 'form-control', 'placeholder'=>'Phone','id' => 'readinput', 'required')) }}
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success btn-sm float-right submit_btn">
                    <i class="fas fa-arrow-alt-circle-right"></i> Save</button>
                <a href='/client/profile' class="btn btn-secondary btn-sm" id="add_more">
                    <i class="fas fa-plus-circle"></i>Cancel</a>
            </div>
            {{ Form::close()  }}
        </div>
    </div>
</dvi>
@endsection