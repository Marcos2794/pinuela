@extends('layouts.app') @section('content')
<dvi class="container h-100">
    <div class="d-flex justify-content-center">
        <div class="card mt-5 col-md-4 animated bounceInDown myForm">
            <div class="card-header">
                <h4>Profile</h4>
            </div>
            <div class="card-body">
                {{ Form::model($user, array('route' => array('profile.update',auth()->user()->id), 'method' => 'PUT')) }}
                @csrf
                <div id="dynamic_container">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                            <i class="fas fa-key"></i>
                            </span>
                        </div>
                        <input id="password" type="password" placeholder="Password"
                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                            required> @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text br-15">
                            <i class="fas fa-lock-open"></i>
                            </span>
                        </div>
                        <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control"
                            name="password_confirmation" required>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-success btn-sm float-right submit_btn">
                    <i class="fas fa-arrow-alt-circle-right"></i> Save</button>
                <a href='/client/profile' class="btn btn-secondary btn-sm" id="add_more">
                    <i class="fas fa-plus-circle"></i>Cancel</a>
            </div>
            {{ Form::close()  }}
        </div>
    </div>
</dvi>
@endsection