@extends('layouts.app') @section('content')
<dvi class="container h-100">
	<div class="d-flex justify-content-center">
		<div class="card mt-5 col-md-4 animated bounceInDown myForm">
			<div class="card-header">
				<h4>Profile</h4>
			</div>
			<div class="card-body">
            <form method="">
				<div id="dynamic_container">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
								<i class="fas fa-user"></i>
							</span>
						</div>
						{{ Form::text('name',auth()->user()->name, array('class' => 'form-control','readonly','id' => 'readinput', 'required')) }}
                    </div>
                    <div class="input-group mt-3">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
								<i class="fas fa-at"></i>
							</span>
						</div>
						{{ Form::text('email', auth()->user()->email, array('class' => 'form-control','readonly','id' => 'readinput', 'required')) }}
					</div>
					<div class="input-group mt-3">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
							<i class="fas fa-globe-americas"></i>
							</span>
						</div>
						{{ Form::text('country', auth()->user()->country, array('class' => 'form-control', 'readonly','id' => 'readinput', 'required')) }}
					</div>
					<div class="input-group mt-3">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
							<i class="fas fa-phone"></i>
							</span>
						</div>
						{{ Form::text('phone', auth()->user()->phone, array('class' => 'form-control', 'readonly','id' => 'readinput', 'required')) }}
                    </div>
				</div>
			</div>
			<div class="card-footer">
            <a href='/client/profile/{{auth()->user()->id}}/edit' class="btn btn-secondary btn-sm" id="add_more">
                    <i class="fas fa-plus-circle"></i>Edit Profile</a>
            <a href='/client/profile/{{auth()->user()->id}}' class="btn btn-secondary btn-sm float-right" id="add_more">
            <i class="fas fa-plus-circle"></i>Edit Password</a>
			</div>
			</form>
		</div>
	</div>
</dvi>
@endsection
