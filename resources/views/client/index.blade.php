@extends('layouts.app')
<title>Client</title>
    @section('content')
    <!-- Banner -->
<section id="banner">
	<div class="inner">
		<h2>Piñuela Treks</h2>
		<p>Nature is calling you</p>
		<p>As one of our loved customers you could see the information whenever a whatever you are just logging!</p>
		<a href="/client/backpack" class="btn btn-sm  btn-info"><img src="img/index/backpack1.png" alt="" /> go to backpack</a>
		
</section>

<!-- One -->
<section id="one" class="wrapper style1 special">
	<div class="inner">
		<header class="major">
			<h2>“All you got to do is decide to go and <br/> the hardest part is over. So, go!”
				<br />  Tony Wheeler</h2>
		</header>
		<ul class="icons major">
			<li>
				<span style="font-size: 3em;">
					<i class="fas fa-frog fa-2xs fa-pull-right fa-border"></i>
				</span>
			</li>
			<li>
				<span style="font-size: 3em; color: Green;">
					<i class="fab fa-pagelines fa-2xs fa-pull-right fa-border"></i>
				</span>
			</li>
			<li>
				<span style="font-size: 3em; color: Dodgerblue;">
					<i class="fas fa-water fa-2xs fa-pull-right fa-border"></i>
				</span>
			</li>
		</ul>
	</div>
</section>

<!-- Two -->
<section id="two" class="wrapper alt style2">
	<section class="spotlight">
		<div class="image">
			<img src="img/index/pic01.jpg" alt="" />
		</div>
		<div class="content">
			<h2>TRAPICHE</h2>
			<p>Ávila and Marín is the farm where oxherding and oxcart traditions still alive. This family has practiced these traditions
				for about fifty years. Oxherding was inscribed as Intangible Cultural World Heritage by UNESCO in 2008.</p>
		</div>
	</section>
	<section class="spotlight">
		<div class="image">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/azSc2Qd1ov0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
			 allowfullscreen></iframe>
		</div>
		<div class="content">
			<h2>LOS SALTOS</h2>
			<p>Surrounded by lush vegetation between spectacular cliffs. Inspiration comes from everywhere you look. An experience of
				100% oxygen. </p>
		</div>
	</section>
	<section class="spotlight">
		<div class="image">
			<img src="img/index/pic03.jpg" alt="" />
		</div>
		<div class="content">
			<h2>NATURE</h2>
			<p>Reduce stress and increase energy while you go into unspoiled forests. From mountains get stunning views of beach. Monkeys,
				toucans, hummingbirds, sloths, coatis, iguanas and other species live in these <br />  best-kept habits.</p>
		</div>
	</section>
</section>

<!-- Three -->
<section id="three" class="wrapper style3 special">
	<div class="inner">
		<header class="major">
			<h2>Services</h2>
			<p>You deserve the best experience. Please ask about any requirement or service you need.
				<br />  We are going to help you on anything we can.</p>
		</header>
		<ul class="features">
			<li>
			<h3><i class="fas fa-car"></i> Mus Scelerisque</h3>
				<p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
			</li>
			<li>
				<h3><i class="fas fa-shower"></i> Mus Scelerisque</h3>
				<p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
			</li>
			<li>
			<h3><i class="fas fa-bed"></i> Mus Scelerisque</h3>
				<p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p>
			</li>
		</ul>
	</div>
</section>
<embed style="background-color: transparent;" src="{!! asset('audio/mono.mp3') !!}" autostart="true" loop="false" width="150"
	 				height="20" HIDDEN="true">
@endsection

    