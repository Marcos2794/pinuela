<!DOCTYPE HTML>
<!--
	Spectral by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
	<title>Piñuela</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
	 crossorigin="anonymous">

	<!-- My style -->
	
	<link rel="stylesheet" href="{!! asset('css/mymain.css') !!}">
	<link rel="stylesheet" href="{!! asset('css/style.css') !!}">

	<!-- Templade -->
	<link rel="stylesheet" href="{!! asset('css/main.css') !!}" />
	<noscript>
		<link rel="stylesheet" href="{!! asset('css/noscript.css') !!}" />
	</noscript>
</head>

<body class="is-preload">

	<!-- Page Wrapper -->
	<div id="page-wrapper">

		<!-- Header -->
		<header id="header">
			<h1>
				<a href="/">Piñuela</a>
			</h1>
			<nav id="nav">
				<ul class="nav">
					<li>
						<a href="/admin">Home</a>
					</li>
					<li>
						<a href="/admin/reservation">Reservation</a>
					</li>
					<li>
						<a href="/admin/newtour">Tours</a>
					</li>
					<li>
						<a href="/admin/newpackage">Packages</a>
					</li>
					<li>
	                    <a class="dropdown-item" href="{{ route('logout') }}"
	                       onclick="event.preventDefault();
	                                     document.getElementById('logout-form').submit();">
	                        {{ __('Logout') }}
	                    </a>

	                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                        @csrf
	                    </form>
					</li>
				</ul>
			</nav>
		</header>

		<!-- Main -->

		<section>
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			@if(session('success')) 
				<div class="alert alert-warning alert-dismissible fade show" role="alert">
				<strong>Holy Admin!</strong> {{session('success')}}.
				<button type="button" class="close"  id="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
			@endif
			@yield('content')
		</section>

		<!-- Footer -->
		@extends('layouts.footer') @section('content') @endsection