<!DOCTYPE HTML>
<!--
	Spectral by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>

<head>
    <title>Piñuela</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!-- My style -->
    <link rel="stylesheet" href="{!! asset('css/main.css') !!}" />
    <link rel="stylesheet" href="{!! asset('css/style.css') !!}">
    <noscript>
        <link rel="stylesheet" href="{!! asset('css/noscript.css') !!}" />
    </noscript>
</head>

<body class="landing is-preload">
    <!-- Page Wrapper -->
    <div id="page-wrapper">

        <!-- Header -->
        <header id="header" class="alt">
            <h1>
                <a href="/">Piñuela</a>
            </h1>
            <nav id="nav">
                <ul>
                    <li class="special">
                        <a href="#menu" class="menuToggle">
                            <span>Menu</span>
                        </a>
                        <div id="menu">
                            <ul>
                                @guest
                                <li>
                                    <a href="/"><i class="fas fa-campground"></i>Home</a>
                                </li>
                                @else
                                <li>
                                    <a href="/client"><i class="fas fa-campground"></i>Home</a>
                                </li>
                                @endguest
                                <li>
                                    <a href="/tours"><i class="far fa-compass"></i>Tours</a>
                                </li>
                                <li>
                                    <a href="/packages"><i class="far fa-compass"></i>Packages</a>
                                </li>
                                @guest
                                @else
                                <li>
                                    <a href="/client/backpack"><img src="{!! asset('img/index/backpack.png') !!}"
                                            alt="" />Backpack</a>
                                </li>
                                <li>
                                    <a href="/client/profile"><i class="fas fa-address-card"></i></i>Profile</a>
                                </li>
                                @endguest
                                <li>
                                    <a href="/about_us"><i class="fas fa-users"></i></i>About Us</a>
                                </li>
                                @guest
                                @else
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
													document.getElementById('logout-form').submit();">
                                        <i class="fas fa-reply"></i>
                                        {{ __('Logout') }}

                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                                @endguest
                                @guest
                                <li>
                                    <a href="/login"><i class="fas fa-sign-in-alt"></i>Login</a>
                                </li>
                                @endguest
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
        </header>

        <!-- Main -->
        <section>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if(session('success'))
            <div class="alert alert-warning alert-dismissible fade show col-md-8 " role="alert">
                <strong>Tanks!</strong> {{session('success')}}.
                <button type="button" class="close" id="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            @yield('content')
        </section>

        <!-- Footer -->
        @extends('layouts.footer') @section('content') @endsection