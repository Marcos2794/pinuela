@extends('layouts.app')
<title>Tours</title>
@section('content')
<!-- Two -->
<section id="one" class="wrapper style1 special">
    <div class="inner">
        <header class="major">
            <h2>Tours</h2>
            <p>In your ancient genetic is the memory of a primitive desire to explore and discovery new places.
                <br /> Feel active while you conquer the top.</p>
        </header>
    </div>
</section>
<section id="two" class="wrapper alt style2">
    @foreach($tours as $tour)
    @if($tour->status == 1)
    <section class="spotlight">
        <div class="image"><img src="/img/{{$tour->image}}" alt="" style='height:400px' /></div>
        <div class="content">
            <h2>{{$tour->name}}</h2>
            <ul>
                <li style="list-style:none;"><i class="fas fa-hiking"></i> {{$tour->description}}</li>
                <li style="list-style:none;"><i class="fas fa-hiking"></i> Date: {{$tour->day}}</li>
                <li style="list-style:none;"><i class="fas fa-hiking"></i> Spaces: {{$tour->stock}}</li>
                <li style="list-style:none;"><i class="fas fa-hiking"></i> $ {{$tour->price}}</li>

            </ul>
            <a href="/book/{{$tour->id}}" class="btn btn-sm  btn-warning"><i class="fab fa-earlybirds"></i>Book and
                Pay</a>
        </div>
    </section>
    @endif
    @endforeach
</section>
@endsection