@extends('layouts.headeradmin')
<title>Admin</title>
@section('content')

<br>
<br>
<br>
<!--/.main-->
<div class="container">
    <div class="row">
        <div class="col-sm">
            <table class="table table-dark table-bordered">
                <tr>
                    <th class="text-center" colspan="2">
                        <div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-blue"></em>
                            @foreach($totalpackages as $package)
                            <div class="large">{{$package->quantity}} Packages Sold | Total: ${{$package->price}}</div>
                            @endforeach
                        </div>
                    </th>
                </tr>
                <!--aqui forecha-->
                <tr>
                    <th class="text-center">Most Popular Package</th>
                    <th class="text-center">Total Sold</th>
                </tr>
                @foreach($packages as $package)
                <tr>
                    <td class="text-center">{{$package->atraction_name}}</td>
                    <td class="text-center">{{$package->total}}</td>
                </tr>
                @endforeach
                <!--endforeach-->
						</table>
						{{$packages->links() }}
        </div>
        <div class="col-sm">
            <table class="table table-dark table-bordered">
                <tr>
                    <th class="text-center" colspan="2">
                        <div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-orange"></em>
                            @foreach($totaltours as $tour)
                            <div class="large">{{$tour->quantity}} Tours Sold | Total: ${{$tour->price}}</div>
                            @endforeach
                        </div>
                    </th>
                </tr>
                <!--aqui forecha-->
                <tr>
                    <th class="text-center">Most Popular Tour</th>
                    <th class="text-center">Total Sold</th>
                </tr>
                @foreach($tours as $tour)
                <tr>
                    <td class="text-center">{{$tour->atraction_name}}</td>
                    <td class="text-center">{{$tour->total}}</td>
                </tr>
                @endforeach
                <!--endforeach-->
						</table>
						{{$tours->links() }}
        </div>
        <div class="col-sm">
            <table class="table table-dark table-bordered">
                <tr>
                    <th class="text-center" colspan="2">
                        <div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-orange"></em>
                            @foreach($totalusers as $user)
                            <div class="large">{{$user->id}} Users</div>
                            @endforeach
                        </div>
                    </th>
                </tr>
                <!--aqui forecha-->
                <tr>
                    <th class="text-center">Most Popular Country</th>
                    <th class="text-center">Total</th>
                </tr>
                <tr>
								@foreach($users as $user)
                <tr>
                    <td class="text-center">{{$user->country}}</td>
                    <td class="text-center">{{$user->total}}</td>
                </tr>
                @endforeach
                </tr>
                <!--endforeach-->
						</table>
						{{$users->links() }}
        </div>
    </div>
</div>
@endsection