@extends('layouts.headeradmin')
<title>My Tours</title>
@section('content')
<dvi class="container h-100">
    <form class="form-inline d-flex justify-content-center">
        <div class="form-group mx-sm-3 mb-2">
            <input type="password" class="form-control" name="name" id="search" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Search</button>
    </form>
    <br>
    <div class="categoria">
        <div class="table-responsive-sm">
            <div class="table-responsive-sm">
                <table class="table table-dark table-bordered">
                    <tr>
                        <th class="text-center">Client</th>
                        <th class="text-center">Country</th>
                        <th class="text-center">Phone</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Atraction</th>
                        <th class="text-center">Peoples</th>
                        <th class="text-center">Price $</th>
                        <th class="text-center">Guide</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Start</th>
                        <th class="text-center">Reservation Date</th>
                        <th class="text-center"></th>
                    </tr>
                    <!--Cargo los atributos de productos con el foreach-->
                    @foreach($reservations as $reservation)
                    <tr>
                        <td class="text-center">{{$reservation->name}}</td>
                        <td class="text-center">{{$reservation->country}}</td>
                        <td class="text-center">{{$reservation->phone}}</td>
                        <td class="text-center">{{$reservation->email}}</td>
                        <td class="text-center">{{$reservation->atraction_name}}</td>
                        <td class="text-center">{{$reservation->quantity}}</td>
                        <td class="text-center">{{$reservation->price}}</td>
                        <td class="text-center">{{$reservation->guide_name}}</td>
                        <td class="text-center">{{$reservation->day}}</td>
                        <td class="text-center">{{$reservation->hour}}</td>
                        <td class="text-center">{{$reservation->created_at}}</td>
                        <td class="text-center">
                            @if($reservation->paystatus == 0)
                            {!! Form::open(['route' => ['reservation.destroy', $reservation->id], 'method' => 'DELETE'])
                            !!}
                            <button type="btn btn-secondary btn-sm" class="btn btn-secondary btn-sm" id="delete"><i
                                    class="fas fa-trash"></i></button>
                            {!! Form::close() !!}
                            </button>
                            @else
                            <button type="btn btn-secondary btn-sm" class="btn btn-secondary btn-sm" id="check">
                                <i class="fas fa-check"></i></button>
                            @endif
                        </td>

                    </tr>
                    @endforeach
                </table>
                <!--Divisor de pagina-->
                {{$reservations->render() }}
            </div>
        </div>
    </div>
    </div>
</dvi>
@endsection