@extends('layouts.headeradmin')
<title>My Packages</title>
@section('content')
<dvi class="container h-100">
    <div class="categoria">
        <div class="container">
            <table class="table table-dark table-bordered">
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Price $</th>
                    <th class="text-center">Description</th>
                    <th class="text-center">Guide</th>
                    <th class="text-center">Spaces</th>
                    <th class="text-center">Stock</th>
                    <th class="text-center">Hour</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Image</th>
                    <th class="text-center">
                        <a href="newpackage/create" class="btn btn-succes mr-2" id="add_new">
                            <i class="fas fa-plus-circle"></i></a>
                </tr>
                <!--Cargo los atributos de productos con el foreach-->
                @foreach($packages as $package)
                <tr>
                    <td class="text-center">{{$package->name}}</td>
                    <td class="text-center">{{$package->price}}</td>
                    <td class="text-center">{{$package->description}}</td>
                    <!--Busco el nombre del guia con el id del producto-->
                    @foreach($guides as $guide)
                    @if($package->guide_id == $guide->id)
                    <td class="text-center">{{$guide->name}}</td>
                    @endif
                    @endforeach
                    <td class="text-center">{{$package->hour}}</td>
                    <td class="text-center">{{$package->spaces}}</td>
                    <td class="text-center">{{$package->stock}}</td>
                    <td class="text-center">{{$package->day}}</td>
                    @if(1 == $package->status)
                    <td class="text-center">Activo</td>
                    @else
                    <td class="text-center">Desactivo</td>
                    @endif
                    <td class="text-center">
                        <img src="/img/{{$package->image}}" class="imagen" />
                    </td>
                    <td class="text-center">
                        <a href="newpackage/{{$package->id}}/edit" class="btn btn-secondary btn-sm" id="edit">
                            <i class="fas fa-edit"></i></a>
                        {!! Form::open(['route' => ['newpackage.destroy', $package->id], 'method' => 'DELETE']) !!}
                        <button type="btn btn-secondary btn-sm" class="btn btn-secondary btn-sm" id="delete"><i
                                class="fas fa-trash"></i></button>
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </table>
            <!--Divisor de pagina-->
            {{$packages->links() }}
        </div>
    </div>
</dvi>
@endsection