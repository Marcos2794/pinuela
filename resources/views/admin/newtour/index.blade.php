@extends('layouts.headeradmin')
<title>My Tours</title>
@section('content')
<dvi class="container h-100">
    <div class="categoria">
        <div class="container">
            <table class="table table-dark table-bordered">
                <tr>
                    <th class="text-center">Name</th>
                    <th class="text-center">Price $</th>
                    <th class="text-center">Description</th>
                    <th class="text-center">Guide</th>
                    <th class="text-center">Spaces</th>
                    <th class="text-center">Stock</th>
                    <th class="text-center">Hour</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Image</th>
                    <th class="text-center">
                        <a href="newtour/create" class="btn btn-succes mr-2" id="add_new">
                            <i class="fas fa-plus-circle"></i>
                        </a>
                    </th>
                </tr>
                <!--Cargo los atributos de productos con el foreach-->
                @foreach($tours as $tour)
                <tr>
                    <td class="text-center">{{$tour->name}}</td>
                    <td class="text-center">{{$tour->price}}</td>
                    <td class="text-center">{{$tour->description}}</td>
                    <!--Busco el nombre del guia con el id del producto-->
                    @foreach($guides as $guide)
                    @if($tour->guide_id == $guide->id)
                    <td class="text-center">{{$guide->name}}</td>
                    @endif
                    @endforeach
                    <td class="text-center">{{$tour->spaces}}</td>
                    <td class="text-center">{{$tour->stock}}</td>
                    <td class="text-center">{{$tour->hour}}</td>
                    <td class="text-center">{{$tour->day}}</td>
                    @if(1 == $tour->status)
                    <td class="text-center">Activo</td>
                    @else
                    <td class="text-center">Desactivo</td>
                    @endif
                    <td class="text-center">
                        <img src="/img/{{$tour->image}}" class="imagen" />
                    </td>
                    <td class="text-center">
                        <a href="newtour/{{$tour->id}}/edit" class="btn btn-warning btn-sm" id="edit">
                            <i class="fas fa-edit"></i>
                        </a>
                        {!! Form::open(['route' => ['newtour.destroy', $tour->id], 'method' => 'DELETE']) !!}
                        <button type="btn btn-secondary btn-sm" class="btn btn-secondary btn-sm" id="delete"><i
                                class="fas fa-trash"></i></button>
                        {!! Form::close() !!}
                        </button>
                    </td>
                </tr>
                @endforeach
            </table>
            <!--Divisor de pagina-->
            {{$tours->links() }}
        </div>
    </div>
    </div>
</dvi>
@endsection