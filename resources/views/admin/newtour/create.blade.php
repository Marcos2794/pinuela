@extends('layouts.headeradmin')
<title>New Tours</title>
@section('content')
<dvi class="container h-100">
	<div class="d-flex justify-content-center">
		<div class="card mt-5 col-md-4 animated bounceInDown myForm">
			<div class="card-header">
				<h4>New Tours</h4>
			</div>
			<div class="card-body">
				{!! Form::open(['action' => 'NewTourController@store', 'files' => 'true','method' => 'POST','enctype'=>'multipart/form-data']) !!}
				<div id="dynamic_container">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
								<i class="fas fa-signature"></i>
							</span>
						</div>
						{{ Form::text('name', null, array('class' => 'form-control', 'placeholder'=>'Name', 'required')) }}
					</div>
					<div class="input-group mt-3">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
								<i class="fas fa-pen"></i>
							</span>
						</div>
						{{ Form::textarea('description', null, array('class' => 'form-control','style'=>'rows: 6', 'placeholder'=>'Description', 'required'))}}
					</div>
					<div class="input-group mt-3">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
								<i class="fas fa-dollar-sign"></i>
							</span>
						</div>
						{{ Form::number('price', null, array('class' => 'form-control', 'placeholder'=>'Price','min'=>'1', 'required')) }}
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
							<i class="far fa-calendar-alt"></i>
							</span>
						</div>
						{{ Form::date('day', null, array('class' => 'form-control', 'placeholder'=>'day', 'required')) }}
					</div>
					<div class="input-group mt-3">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
							<i class="far fa-clock"></i>
							</span>
						</div>
						{{ Form::time('hour', null, array('class' => 'form-control', 'required')) }}
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
							<i class="far fa-calendar-alt"></i>
							</span>
						</div>
						{{ Form::number('duration', null, array('class' => 'form-control', 'placeholder'=>'Days','min'=>'1', 'required')) }}
					</div>
					<div class="input-group mt-3">
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
								<i class="fas fa-hiking"></i>
							</span>
						</div>
						<select name="guide_id"  class="form-control" id="guide_id" >
                		<option value="guide_id">-- Select Guide --</option>
						@foreach($guides as $guide)
						<option value="{{$guide['id']}}" >{{$guide['name']}}</option>
						@endforeach	
						<div class="input-group-prepend">
							<span class="input-group-text br-15">
								<i class="fas fa-chair"></i>
							</span>
						</div>
						{{ Form::number('spaces', null, array('class' => 'form-control', 'placeholder'=>'Spaces','min'=>'1', 'required')) }}
					</div>
					<input class="spacing-2"  type="file" name="image" accept="image/*" required>
				</div>
			</div>
			<div class="card-footer">
				<button class="btn btn-success btn-sm float-right submit_btn">
					<i class="fas fa-arrow-alt-circle-right"></i> Save</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</dvi>
@endsection