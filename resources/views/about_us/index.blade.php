@extends('layouts.app')
<title>About Us</title>
@section('content')
<article id="main">
	<header>
		<h2>About Us</h2>
		<p></p>
	</header>
	<section class="wrapper style5">
		<div class="inner">

			<h3>I dream come true means work hard</h3>
			<p>We are an entrepreneur group which first enjoy what today offers. Each of us started loving to explore and discover new
				experiences in our communities as we were looking for an opportunity to develop a small business that helps others and
				offers an option of adventures that can motivate people to visit us and have fun while them forget their busy routines.
			</p>
			<hr/>

			<h4>Why us?</h4>
			<p>We believe in tourism as an excellent way to meet people and have friends because we see each tourist first as a person
				who wants to enjoy and who makes a choice trusts in being safe and sound. Your choice worth a lot.</p>

		</div>
	</section>
	<header>
		<h2>Communities</h2>
		<p></p>
	</header>
	<section class="wrapper style5">
		<div class="inner">

			<h3>Pejibaye</h3>
			<p>It is a small and rural town which offers basic services like a gas station, an ATM, currency exchange office, supermarkets,
				bakery, barber shop, bus, clinic, clothing store, café internet, deli, drug store, electronics store, fast-food restaurants,
				plant nursery, furniture maker. The weather in Pejibaye is almost hot but in summer nights are cold. From December to
				April it is sunny and dry which is the perfect moment to go to rivers. Spanish is the official language but that’s isn’t
				a problem because many people understand English.
			</p>

			<hr/>
			<h4>Las Mesas</h4>
			<p>This is also a rural town near to Pejibaye. It is a quiet and perfect place to meet new people because they are friendly.
				It is less hot in comparison with Pejibaye and is windy. Its economy is based on coffee plantation and cattle. Also,
				there are entrepreneur activities like cacao and banana organic plantation. This town is the place where is in one of
				the most beautiful cascades in near areas. There is an important project that supplies water to community.
			</p>
			<hr/>

			<h4>San Miguel</h4>
			<p>It is a traditional rural area where you can learn about honey process and meet an entrepreneur woman who will explain
				you interesting facts and details about it.</p>
			<hr/>

			<h4>Desamparados</h4>
			<p>It is the jewel of nature on the nearest places. Some decades ago, this rural town was known as “Cold Eagle” by its low
				temperatures. The tropical humid forest separates the region from its neighbor, Osa. This forest is perfect for watching
				birds. It is a place unspoiled by mass tourism and is ideal for travelers looking for an authentic travel experience.</p>
		</div>
	</section>

</article>
@endsection