use pinuela;
use pinuela;

select * from guides;

select * from backpacks;

select * from migrations;

select * from atractions;

select * from users;

select * from countries;

SELECT atraction_name, count(atraction_name) as total
 FROM backpacks GROUP BY atraction_name;
     
SELECT atractions.name,count(atractions.id) as total 
FROM atractions
INNER JOIN backpacks 
ON atractions.id=backpacks.atraction_id GROUP BY atractions.name Order BY atractions.id;

INSERT INTO `pinuela`.`guides` (`name`, `speciality`, `lenguaje`) VALUES ('Ramiro Morales', 'Pesca', 'Español');

INSERT INTO `atractions` (`id`,`name`,`price`,`description`,`image`,`type_atraction`,`hour`,`duration`,`guide_id`,`spaces`,`stock`,`day`,`status`,`created_at`,`updated_at`) VALUES (1,'Salto del Avion',80,'Maravillosas vistas, acompañadas de mucha naturaleza. Una catarata iconica de la zona de pejibaye.','1553050927IMG_20140509_114256.jpg','Tour','10:00:00',1,1,10,10,'2019-03-19',1,'2019-03-19 21:02:07','2019-03-19 21:02:07');
INSERT INTO `atractions` (`id`,`name`,`price`,`description`,`image`,`type_atraction`,`hour`,`duration`,`guide_id`,`spaces`,`stock`,`day`,`status`,`created_at`,`updated_at`) VALUES (2,'Lajas',80,'Maravillosas vistas. Una catarata iconica de la zona de pejibaye.','1553050927IMG_20140509_114256.jpg','Tour','10:00:00',1,1,10,10,'2019-03-19',1,'2019-03-19 21:02:07','2019-03-19 21:02:07');
INSERT INTO `atractions` (`id`,`name`,`price`,`description`,`image`,`type_atraction`,`hour`,`duration`,`guide_id`,`spaces`,`stock`,`day`,`status`,`created_at`,`updated_at`) VALUES (3,'Basic12',120,'Tour básico, recorreremos el paradisíaco rio de las lajas, para terminar con una estancia en la hacienda Ávila.','155305160111082011086.JPG','Package','07:00:00',2,1,10,10,'2019-03-19',1,'2019-03-19 21:13:21','2019-03-19 21:13:21');
INSERT INTO `atractions` (`id`,`name`,`price`,`description`,`image`,`type_atraction`,`hour`,`duration`,`guide_id`,`spaces`,`stock`,`day`,`status`,`created_at`,`updated_at`) VALUES (4,'Basic1',120,'Tour prueba, recorreremos el paradisíaco rio de las lajas.','155305160111082011086.JPG','Package','07:00:00',2,1,10,10,'2019-03-19',1,'2019-03-19 21:13:21','2019-03-19 21:13:21');





/*  backpacks help */
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (1,'',0,'','','','','','Salto',NULL,0,1,120.00,'','','','Tour','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (2,'',0,'','','','','','Salto',NULL,0,1,120.00,'','','','Tour','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (3,'',0,'','','','','','Lajas',NULL,0,1,120.00,'','','','Tour','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (4,'',0,'','','','','','Lajas',NULL,0,1,120.00,'','','','Tour','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (5,'',0,'','','','','','Lajas',NULL,0,1,120.00,'','','','Tour','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (6,'',0,'','','','','','Lajas',NULL,0,1,120.00,'','','','Tour','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (7,'',1,'','','','','','Lajas',NULL,0,1,120.00,'','','','Package','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (8,'',1,'','','','','','Lajas',NULL,0,1,120.00,'','','','Package','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (9,'',1,'','','','','','Lajas',NULL,0,1,120.00,'','','','Package','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (10,'',1,'','','','','','Lajas',NULL,0,1,120.00,'','','','Package','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (11,'',1,'','','','','','Lajas',NULL,0,1,120.00,'','','','Package','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (12,'',1,'','','','','','Montaña',NULL,0,1,120.00,'','','','Package','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (13,'',1,'','','','','','Rio',NULL,0,1,120.00,'','','','Tour','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (14,'',1,'','','','','','Lago',NULL,0,1,120.00,'','','','Tour','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (15,'',1,'','','','','','Trapiche',NULL,0,1,120.00,'','','','Tour','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (16,'',1,'','','','','','Toros',NULL,0,1,120.00,'','','','Package','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (17,'',1,'','','','','','Pericos',NULL,0,1,120.00,'','','','Package','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (18,'',1,'','','','','','Montaña',NULL,0,1,120.00,'','','','Package','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (19,'',1,'','','','','','Rio',NULL,0,1,120.00,'','','','Tour','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (20,'',1,'','','','','','Lago',NULL,0,1,120.00,'','','','Tour','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (21,'',1,'','','','','','Lapas',NULL,0,1,120.00,'','','','Tour','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (22,'',1,'','','','','','Avion',NULL,0,1,120.00,'','','','Package','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (23,'',1,'','','','','','Pericos',NULL,0,1,120.00,'','','','Package','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (24,'',1,'','','','','','Senderos',NULL,0,1,120.00,'','','','Package','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (25,'',0,'','','','','','Rio',NULL,0,1,120.00,'','','','Tour','2019-05-05 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (26,'',1,'','','','','','Lago',NULL,0,1,120.00,'','','','Tour','2019-03-19 21:13:21',NULL);
INSERT INTO `backpacks` (`id`,`name`,`paystatus`,`country`,`phone`,`email`,`description`,`guide_name`,`atraction_name`,`user_id`,`atraction_id`,`quantity`,`price`,`duration`,`hour`,`image`,`type_atraction`,`created_at`,`updated_at`) VALUES (37,'',0,'','','','','','',NULL,0,0,0.00,'','','','','2019-05-05 21:13:21',NULL);



