<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//route welcome
Route::get('/',function(){
    return view('welcome');
});

//verified routes
Auth::routes(['verify'=> true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/auth/register', 'Auth\RegisterController@index')->name('register');

//route book 
Route::prefix('book')->group(function () {
  Route::resource('book', 'ReservationController');
});

//logout route
Route::get('logout', 'Auth\LoginController@logout');

//route tours 
Route::get('/tours', 'TourController@index')->name('tours');

//route about us 
Route::get('/about_us', 'AboutController@index')->name('about_us');

//route services 
Route::get('/services', 'ServiceController@index')->name('services');

//route packages 
Route::get('/packages', 'PackageController@index')->name('packages');
Route::get('packages/{id}','PackageController@show',function ($id){ 
  return 'packages'.$id;
});

/* -------- ADMINS -------------*/
/* -------- ADMINS -------------*/
/* -------- ADMINS -------------*/
/* -------- ADMINS -------------*/

//route admin 
Route::get('/admin', 'AdminController@index')->name('admin')->middleware('verified');

//admin/reservation
Route::prefix('admin')->group(function () {
  Route::resource('reservation', 'AdminReservationController')->middleware('verified');
});

//admin/packages
Route::prefix('admin')->group(function () {
    Route::resource('newpackage', 'NewPackageController')->middleware('verified');
    Route::get('newpackage/create','NewPackageController@create')->middleware('verified');
    Route::get('newpackage/edit','NewPackageController@edit')->middleware('verified');
  });

//admin/tours
Route::prefix('admin')->group(function () {
    Route::resource('newtour', 'NewTourController')->middleware('verified');
    Route::get('newtour/create','NewTourController@create')->middleware('verified');
    Route::get('newtour/edit','NewTourController@edit')->middleware('verified');
  });

/* -----------------CLIENT-------------*/  
/* -----------------CLIENT-------------*/  
/* -----------------CLIENT-------------*/  


//route client 
Route::get('/client', 'ClientController@index')->name('client');


//route client/backpack
Route::prefix('client')->group(function () {
  Route::resource('backpack', 'BackpackController')->middleware('verified');
  Route::get('/client/backpack/{id}','BackpackController@show',function ($id){ 
    return 'atractions'.$id;
  })->middleware('verified');
});

//route client/profile
Route::prefix('client')->group(function () {
  Route::resource('profile', 'ProfileController')->middleware('verified');
  Route::get('profile/{id}','ProfileController@show')->middleware('verified');
  Route::get('profile/edit','ProfileController@edit')->middleware('verified');
});

//route client for book 

Route::get('book/{id}','ReservationController@index',function ($id){ 
  return 'packages'.$id;
});


/* -------- PAYPAL -------------*/
/* -------- PAYPAL -------------*/
/* -------- PAYPAL -------------*/
/* -------- PAYPAL -------------*/

  // route for processing payment
Route::post('paypal', 'PaypalController@payWithpaypal');

// route for check status of the payment
Route::get('status/{atraction_id}', 'PaypalController@getPaymentStatus');

// route for check status of the payment

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// forgot password

Route::get('/reset','Auth\ForgotPasswordController@showLinkRequestForm');
Route::get('auth/passwords/reset/{token}','Auth\ResetPasswordController@showResetForm');