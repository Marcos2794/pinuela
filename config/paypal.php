<?php
return array(
    // set your paypal credential
    'client_id' => 'AQ7yWhfxHVfCb3L3PtdRKBw6fXr_c929E1bbpEh6iPZTxlvA0DdmGe_kg_1zbH-UsAnhuQ3To5qRMcVh',
    'secret' => 'EIuxa8CeW1W1LMktEbCfAgynb2t9axQqbBtd0dlJ1LhKTZRMeW9W352WsCfrqS74hWCcy3IhTjqemnSu',
    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);